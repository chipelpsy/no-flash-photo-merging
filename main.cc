#include "include.h"

extern Mat joint_bilateral(Mat , Mat , int, double, double);
extern Mat image_registration(Mat, Mat, int, double);
extern Mat create_mask(Mat ,Mat , float, float, float);

int main(int argc, char *argv[]){
  if(argc != 3){
    printf("usage: DisplayImage.out <No flash image path> <Flash image path>\n");
    return -1;
  }


  Mat nfim, fim, outim, wbim, fl, nfl,mask,redeyeout;
  nfim = imread(argv[1], CV_LOAD_IMAGE_COLOR);
  fim = imread(argv[2], CV_LOAD_IMAGE_COLOR);

  if (nfim.data == NULL || fim.data == NULL){
    printf("Image didn't load\n");
    exit(1);
  }

  // fit image onto 1920 1080 screen
  int new_w, new_h, old_w, old_h;
  old_h = nfim.rows;
  old_w = nfim.cols;
  if(old_w*9 >= old_h*16){
    new_w = 1920;
    new_h = old_h*new_w/old_w;
  }
  else{
    new_h = 1080;
    new_w = old_w*new_h/old_h;
  }
  
  // resize to speed up computation
  Size sz(new_w, new_h);
  resize(nfim, nfim, sz);
  resize(fim, fim, sz);

  printf("Image registration...\t");fflush(stdout);
  try{
    fim = image_registration(nfim, fim, 100, 0.000001);
    printf("done\n");
  }
  catch(Exception e){
    printf("failed\n");
  }
  namedWindow("Display Image", WINDOW_AUTOSIZE);


  // detail transfer
  {
    Mat ANR, Abase, Abase_float,Fbase, Fdetail, ANR_float, fim_float, nfim_float, Fbase_float, interim;
    float globalthreshold, noise_threshold, specThresh;
    globalthreshold = 10.0;
    noise_threshold = 40.0;
    specThresh = 0.9;
    
    printf("Computing A^{NR}...\t");fflush(stdout);
    ANR = joint_bilateral(nfim, fim, 7, 8, 2);
    if(ANR.data == NULL){
      printf("Filtering failed\n");
      exit(1);
    }
    printf("done\n");
  
    printf("Computing A^{base}...\t");fflush(stdout);
    Abase= joint_bilateral(nfim, nfim, 5, 5, 7);
    if(Abase.data == NULL){
      printf("Filtering failed\n");
      exit(1);
    }
    printf("done\n");
  
    printf("Computing F^{base}...\t");fflush(stdout);
    Fbase = joint_bilateral(fim, fim, 5, 7, 4);
    if(Fbase.data == NULL){
      printf("Filtering failed\n");
      exit(1);
    }
    printf("done\n");

    // transfer detail from flash to no-flash
    fim.convertTo(fim_float, CV_32F, 1, 3);
    nfim.convertTo(nfim_float, CV_32F, 1, 3);
    Fbase.convertTo(Fbase_float, CV_32F, 1, 3);
    ANR.convertTo(ANR_float, CV_32F);
    Fdetail = fim_float/Fbase_float;
    Abase.convertTo(Abase_float,CV_32F);
    interim = ANR_float.mul(Fdetail);

    // get user defined threshold
    int done, state;
    done = 0;
    state = 1;
    while(!done){
      Mat tmp1, tmp2, l3[3];

      if(specThresh < -0.05)
	specThresh = -0.05;
      if(specThresh > 1)
	specThresh = 1;
      if(noise_threshold < 0)
	noise_threshold = 0;
      if(noise_threshold > 255)
	noise_threshold = 255;
      
      mask = create_mask(fim_float,nfim_float,globalthreshold, noise_threshold, specThresh);
      l3[0] = l3[1] = l3[2] = mask;
      merge(l3, 3, tmp1);
      l3[0] = l3[1] = l3[2] = 1 - mask;
      merge(l3, 3, tmp2);
      outim = tmp2.mul(interim) + tmp1.mul(Abase_float);

      if(state)
	imshow("Display Image", tmp1);
      else
	imshow("Display Image", outim/255);

      done = 0;
      switch((waitKey(0) + 256)%256){
      case 81:
	globalthreshold -= 3;
	break;
      case 83:
	globalthreshold += 3;
	break;
      case 182:
	specThresh += 0.02;
	break;
      case 180:
	specThresh -= 0.02;
	break;
      case 184:
	noise_threshold += 10;
	break;
      case 178:
	noise_threshold -= 10;
	break;
      case 82:
      case 84:
	state ^= 1;
	break;
      case '\n':
	done = 1;
	break;
      case 255:
      case 27:
	return 0;
      }
    }
  }

  // white balance
  {
    Mat delt, fim_float, c, c_bgr[3], a_bgr[3], oim[3];
    double mn, mx;
    
    fim.convertTo(fim_float, CV_32F);
    delt = fim_float - outim;
    minMaxIdx(delt, &mn, &mx);
    delt = (delt - mn)/(mx - mn);
    imshow("Display Image", delt);
    waitKey(0);
    
    c = outim/delt;

    split(c, c_bgr);
    split(outim, a_bgr);

    int i;
    for(i = 0;i < 3;i++){
      blur(c_bgr[i], c_bgr[i], Size(10000, 10000));
      oim[i] = a_bgr[i]/c_bgr[i];
    }
    merge(oim, 3, wbim);
    //    wbim = wbim*(mx - mn) + mn;
  }

  // Flash Adjust
  {
    outim.convertTo(nfl, CV_32F);
    fim.convertTo(fl, CV_32F);
  }


  //Red Eye Correction
  {
  	Mat fim_ycbcr, nfim_ycbcr, fim_cr[3], nfim_cr[3], redness, redmask,msk1,msk2;
  	Scalar meanR, stddevR,maxVal; 
  	cvtColor(fim, fim_ycbcr, CV_BGR2YCrCb);
  	cvtColor(nfim, nfim_ycbcr, CV_BGR2YCrCb);
  	split(fim_ycbcr, fim_cr);
  	split(nfim_ycbcr, nfim_cr);
  	redness = fim_cr[2] - nfim_cr[2];
  	//merge(fim_cr,3,redeyeout);
  	compare(redness, 0.05, redmask, CMP_GT);
  	meanStdDev(redness, meanR, stddevR);
  	//cout << meanR(0) << endl;
  	add(meanR, stddevR, maxVal);
  	max(0.6, maxVal,maxVal);
  	/*cout << maxVal << endl;*/
  	compare(nfim_cr[0],0.6, msk1, CMP_LT);
  	compare(redness,maxVal(0), msk2, CMP_GT);
  	bitwise_and(msk1,msk2,msk1);
  	redeyeout = redness;
  }
  
  double alpha;
  int state;
  alpha = 0;
  state = 0;

  while(142857){
    Mat toshow;

    switch(state){
    case 0:
      toshow = outim/255;
      break;
    case 1:
      toshow = (wbim)/3;
      break;
    case 2:
      toshow = ((1-alpha)*nfl + alpha*fl)/255;
      break;
    }
    
    imshow("Display Image", toshow);
    
    switch((waitKey(0) + 256)%256){
    case 81:
      if(state == 2)
	alpha -= 0.02;
      break;
    case 82:
      state = (state + 2)%3;
      break;
    case 83:
      if(state == 2)
	alpha += 0.02;
      break;
    case 84:
      state = (state + 1)%3;
      break;
    case 255:
    case 27:
      return 0;
    }
  }

  return 0;
}
