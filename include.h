#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<opencv2/opencv.hpp>
#include<pthread.h>

using namespace cv;

typedef struct __st_bilat_arg_{
  Mat im1, im2, out_im;
  int w_size, tnum, tid;
  double s_d, s_c;
}BILAT_ARG;
