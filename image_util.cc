#include "include.h"

Mat joint_bilateral(Mat, Mat, int, double, double);
void *joint_bilat_func(void *);
Mat image_registration(Mat, Mat, int, double);
Mat create_mask(Mat ,Mat, float, float, float);

Mat joint_bilateral(Mat im1, Mat im2, int w_size, double s_d, double s_c){
  int n_r, n_c, i, num_threads;
  Mat toret;
  BILAT_ARG arg[16];
  pthread_t tid[16];

  if(im1.channels() == 3){
    Mat im1_bgr[3], im2_bgr[3], out_bgr[3];
    split(im1, im1_bgr);
    split(im2, im2_bgr);
    
    // Operate on each channel
    out_bgr[0] = joint_bilateral(im1_bgr[0], im2_bgr[0], w_size, s_d, s_c);
    out_bgr[1] = joint_bilateral(im1_bgr[1], im2_bgr[1], w_size, s_d, s_c);
    out_bgr[2] = joint_bilateral(im1_bgr[2], im2_bgr[2], w_size, s_d, s_c);

    // Merge results and return
    merge(out_bgr, 3, toret);
    return toret; 
  }

  CV_Assert(im1.depth() == CV_8U);
  CV_Assert(im2.depth() == CV_8U);

  n_r = im1.rows;
  n_c = im1.cols;
  s_d *= s_d;
  s_c *= s_c;
  if(w_size > 100){
    // Size not supported
    printf("FAIL: window size too large\n");
    exit(1);
  }
  if(n_r != im2.rows || n_c != im2.cols){
    printf("Sizes dont match (%d, %d) and (%d, %d)\n", n_r, n_c, im2.rows, im2.cols);
    exit(1);
  }
  toret = cv::Mat(n_r, n_c, CV_8U);

  num_threads = 8;
  for(i = 0;i < num_threads;i++){
    arg[i].im1 = im1;
    arg[i].im2 = im2;
    arg[i].out_im = toret;
    arg[i].w_size = w_size;
    arg[i].s_d = s_d;
    arg[i].s_c = s_c;
    arg[i].tid = i;
    arg[i].tnum = num_threads;
    pthread_create((tid + i), NULL, joint_bilat_func, (void *)(arg + i));
  }
  for(i = 0;i < num_threads;i++){
    pthread_join(tid[i], NULL);
  }
  
  return toret;
}

void *joint_bilat_func(void *arg){
  Mat im1, im2, toret;
  int i, j, n_r, n_c, w_size, tid, tnum;
  double c_diff[256][256], d_diff[101][101], s_c, s_d;
  BILAT_ARG blarg;

  // get arguments
  blarg = *(BILAT_ARG *)arg;
  im1 = blarg.im1;
  im2 = blarg.im2;
  toret = blarg.out_im;
  w_size = blarg.w_size;
  tnum = blarg.tnum;
  tid = blarg.tid;
  s_c = blarg.s_c;
  s_d = blarg.s_d;
  n_r = im1.rows;
  n_c = im1.cols;

  // Precompute invexp
  for(i = 0;i < 256;i++){
    for(j = i;j < 256;j++){
      c_diff[i][j] = exp(((double)(i-j))/s_c);
      c_diff[j][i] = c_diff[i][j];
    }
  }
  for(i = 0;i < 101;i++){
    for(j = i;j < 101;j++){
      d_diff[i][j] = exp(((double)(i*i+j*j))/s_d);
      d_diff[j][i] = d_diff[i][j];
    }
  }
  
  // Joint filtering 
  for(i = tid;i < n_r;i += tnum){
    for(j = 0;j < n_c;j++){
      int px, py;
      double wts, wt, val;
      val = 0;
      wts = 0;
      for(px = std::max(0, i - w_size);px <= i + w_size && px < n_r;px++){
	for(py = std::max(0, j - w_size);py <= j + w_size && py < n_c;py++){
	  wt = c_diff[im2.at<uchar>(px, py)][im2.at<uchar>(i, j)] * d_diff[std::abs(i - px)][std::abs(j - py)];
	  val += wt*im1.at<uchar>(px, py);
	  wts += wt;
	}
      }
      toret.at<uchar>(i, j) = (char)(val/wts);
    }
  }

  return NULL;
}

Mat image_registration(Mat im1, Mat im2, int num_iter, double eps){
  Mat im1_gray, im2_gray, warp_matrix, toret;
  
  cvtColor(im1, im1_gray, CV_BGR2GRAY);
  cvtColor(im2, im2_gray, CV_BGR2GRAY);
  warp_matrix = Mat::eye(2, 3, CV_32F);

  TermCriteria criteria(TermCriteria::COUNT|TermCriteria::EPS, num_iter, eps);
  findTransformECC(im1_gray, im2_gray, warp_matrix, MOTION_EUCLIDEAN, criteria);
  warpAffine(im2, toret, warp_matrix, im1.size(), INTER_LINEAR|WARP_INVERSE_MAP);

  return toret;
}

Mat create_mask(Mat im1,Mat im2, float gThreshold, float noiseThresh, float specThresh){
  Mat outputMask, im1_gray, im2_gray, msk1, msk2, msk3;
  int st_elem, st_size;
  st_elem = 2;
  cvtColor(im1, im1_gray, CV_BGR2GRAY);
  cvtColor(im2, im2_gray, CV_BGR2GRAY);

  compare(im1_gray - im2_gray, gThreshold, msk1, CMP_LE);
  compare(im1_gray, noiseThresh, msk2, CMP_LE);
  compare(im1_gray, specThresh*255, msk3, CMP_GE);
  outputMask = (msk1 & msk2) | msk3;
  int st_type;
  if(st_elem == 0)st_type = MORPH_RECT;
  else if(st_elem == 1)st_type = MORPH_CROSS;
  else if(st_elem == 2)st_type = MORPH_ELLIPSE;

  Mat element;

  /// Apply the erosion operation
  st_size = 5;
  element = getStructuringElement(st_type, Size(2*st_size + 1,2*st_size + 1), Point(st_size, st_size));
  erode(outputMask, outputMask, element);
  st_size = 9;
  element = getStructuringElement(st_type, Size(2*st_size + 1,2*st_size + 1), Point(st_size, st_size));
  dilate(outputMask, outputMask, element);
  blur(outputMask, outputMask, Size(11, 11));
  outputMask.convertTo(outputMask, CV_32F);
  return (outputMask)/255;
}

// Mat loadPfm(const char * filename){
//    char strPF[3];
//   int SizeX;
//   int SizeY;
//   float dummy;
//   int dummyC;
 
//   FILE *file = fopen(filename, "rb");
 
//   if(file == NULL){
//     printf("PFM-File not found!\n");
//     exit(1);
//   }
 
//   fscanf(file, "%s\n%d %d\n", strPF, &SizeX, &SizeY);
//   dummyC = fgetc(file);
//   fscanf(file, "\n%f\n", &dummy);
 
//   //DEBUG Ausgabe
//   printf("Keyword: %s\n",strPF);
//   printf("Size X: %d\n", SizeX);
//   printf("Size Y: %d\n", SizeY);
//   printf("dummy: %f\n", dummy);
//   // ENDE Debug Ausgabe
  
//   int result;
//   int lSize;
//   lSize = mSizeX*3;
//   for(int y=mSizeY-1; y>=0; y--){
//     result = fread(data+1+mSizeX*y, sizeof(float), lSize, file);
//     if (result != lSize) {
//       printf("Error reading PFM-File. %d Bytes read.\n", result);
//     }
//   }
 
//   fclose(file);
// }
